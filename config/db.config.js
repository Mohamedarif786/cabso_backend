const Sequelize = require('sequelize');

// const DBName = 'diamondback_dbkadmin';
// const DBLogin = 'diamondback_dbkadmin'
// const DBPassword = 'TGw]GpvCo{~2'
// const DBServer = 'localhost'

const DBName = 'cabso';
const DBLogin = 'root'
const DBPassword = ''
const DBServer = 'localhost'

// Option 1: Passing parameters separately
const sequelize = new Sequelize(DBName, DBLogin, DBPassword, {
    host: DBServer,
    port: 3306,
    dialect: 'mysql',
    logging: console.log
});

sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully. 🚀');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });

module.exports = sequelize;

