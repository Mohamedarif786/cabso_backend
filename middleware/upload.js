const multer = require("multer");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    console.log("fieldname",file.fieldname)
    cb(null, 'public/images/')
  },
  filename: function (req, file, cb) {
    // const name = file.originalname.split(' ').join('_');
    cb(null,Date.now()+file.originalname)
  }
})
const upload = multer({storage: storage})
module.exports = upload;