const jwt = require('jsonwebtoken');

var auth = (req,res,next) => {
    const token = req.header('Authorization');
    console.log('token',token);
    if(!token){ return res.status(401).send('Access Denied'); }

    try{
        console.log("verifyToken-Try");
        const verified = jwt.verify(token, process.env.JWT_SECRET);
        console.log('verified',verified);
        next();
    } catch(err) {
        console.log("verifyToken-Catch");
        res.status(200).send('Invalid Token')
    }
}

module.exports = auth;