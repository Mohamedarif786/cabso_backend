const { rawListeners } = require("../app");
const home_page_modal = require("../model/static_homepage.model");
const static_driver_homepage =require("../model/static_driverhomepage.model");
const fs =require("fs");

const create = async (req, res, next) => {
  try {
    console.log("data" , JSON.parse(req.body.data));
    let json_obj = JSON.parse(req.body.data);
    console.log("image" , req.file.path);

    if (req.file == undefined) {
      return res.send(`You must select a file.`);
    } 
     
    let data ={
      title:json_obj.title,
      content:json_obj.content,
      image: req.file.path,
      user_id:json_obj.user_id
    }

    let home_page = await home_page_modal.create(data);


    if (!home_page) {
      res.status(200).json({
        status: false,
        message: "home_page Creation Failed",
      });
      return res;
    }

    res.status(200).json({
      status: true,
      message: "home_page Created Successfully",
    });
    return res;
  } catch (err) {
    res.status(409).send({
      status: false,
      message:
        err && err.original && err.original.message
          ? err.original.message
          : "Error",
    });
  }
};

const read = async (req, res, next) => {
  try {
    let home_page_get = await home_page_modal.findAll();

    if (!home_page_get) {
      res.status(200).json({
        status: false,
        message: "home_page get Failed",
      });
      return res;
    }

    res.status(200).json({
      status: true,
      message: "home_page get Successfully",
      data: home_page_get,
    });
    return res;
  } catch (err) {
    res.status(409).send({
      status: false,
      message:
        err && err.original && err.original.message
          ? err.original.message
          : "Error",
    });
  }
};

const update = async (req, res, next) => {
  try {
    // console.log("data" , JSON.parse(req.body.data));
    let json_obj = JSON.parse(req.body.data);

    if (req.file == undefined) {
      // return res.send(`You must select a file.`);
      let update_val ={
        title:json_obj.title,
        content:json_obj.content,
        user_id:json_obj.user_id
      }
  
      console.log(update_val);
      
      let home_page_update = await home_page_modal.update(update_val, {
        where: { id: json_obj.id }, 
      });


      if (!home_page_update) {
        res.status(200).json({
          status: false,
          message: "Home_page Update Failed",
        });
        return res;
      }
      if (home_page_update) {
        res.status(200).json({
          status: true,
          message: "Home_page Update Successfully",
        });
        return res;
      }
      
    }else{
      let find_image =await home_page_modal.findOne({
        where:{
          id:json_obj.id,
        }  
      });
      let path =find_image.image;
  
      if (fs.existsSync(path)){
        fs.unlinkSync(path);
        console.log("Image Deleted Successfully");
      }
      
      let update_val ={
        title:json_obj.title,
        content:json_obj.content,
        image: req.file.path,
        user_id:json_obj.user_id
      }
  
      console.log(update_val);
      
      let home_page_update = await home_page_modal.update(update_val, {
        where: { id: json_obj.id }, 
      });


      if (!home_page_update) {
        res.status(200).json({
          status: false,
          message: "Home_page Update Failed",
        });
        return res;
      }
      if (home_page_update) {
        res.status(200).json({
          status: true,
          message: "Home_page Update Successfully",
        });
        return res;
      }
    }
  
  } catch (err) {
    res.status(409).send({
      status: false,
      message: err,
    });
  }
};

const home_page_delete = async (req, res, next) => {
  try {
    let find_image =await home_page_modal.findOne({
      where:{
        id:req.body.id,
      }  
    });
    let path =find_image.image;

    if (fs.existsSync(path)){
      fs.unlinkSync(path);
      console.log("image Deleted successfully");
    }

    const delete_home_page = await home_page_modal.destroy({
      where: { id: req.body.id },
    });
    console.log("delete id" + req.body.id);
    if (!delete_home_page) {
      res.status(200).json({
        status: false,
        message: "home_page delete failed",
      });
      return res;
    }

    res.status(200).json({
      status: true,
      message: "home_page delete Successfully",
    });
    return res;
  } catch (err) {
    res.status(409).send({
      status: false,
      message:
        err && err.original && err.original.message
          ? err.original.message
          : "Error",
    });
  }
};

const driver_home_create =async(req,res)=>{
  try {
    console.log("data" , JSON.parse(req.body.data));
    let json_obj = JSON.parse(req.body.data);
    console.log("image" , req.file.path);

    if (req.file == undefined) {
      return res.send(`You must select a file.`);
    } 
     
    let data ={
      title:json_obj.title,
      content:json_obj.content,
      image: req.file.path,
      user_id:json_obj.user_id
    }

    let driver_home_page_create = await static_driver_homepage.create(data);


    if (!driver_home_page_create) {
      res.status(200).json({
        status: false,
        message: "home_page Creation Failed",
      });
      return res;
    }

    res.status(200).json({
      status: true,
      message: "home_page Created Successfully",
    });
    return res;
  } catch (err) {
    res.status(409).send({
      status: false,
      message:
        err && err.original && err.original.message
          ? err.original.message
          : "Error",
    });
  }
};

const driver_home_read = async (req, res, next) => {
  try {
    let driver_home_page_get = await static_driver_homepage.findAll();

    if (!driver_home_page_get) {
      res.status(200).json({
        status: false,
        message: "home_page get Failed",
      });
      return res;
    }

    res.status(200).json({
      status: true,
      message: "home_page get Successfully",
      data: driver_home_page_get,
    });
    return res;
  } catch (err) {
    res.status(409).send({
      status: false,
      message:
        err && err.original && err.original.message
          ? err.original.message
          : "Error",
    });
  }
};

const driver_home_update = async (req, res, next) => {
  try {
    // console.log("data" , JSON.parse(req.body.data));
    let json_obj = JSON.parse(req.body.data);
    if (req.file == undefined) {
      let update_val ={
        title:json_obj.title,
        content:json_obj.content,
        user_id:json_obj.user_id
      }

      console.log(update_val);
      
      let driver_home_page_update = await static_driver_homepage.update(update_val, {
        where: { id: json_obj.id }, 
      });

      if (!driver_home_page_update) {
        res.status(200).json({
          status: false,
          message: "home_page update Failed",
        });
        return res;
      }
      if (driver_home_page_update) {
        res.status(200).json({
          status: true,
          message: "home_page update Successfully",
        });
        return res;
      }


    }else{

      let find_image =await static_driver_homepage.findOne({
        where:{
          id:json_obj.id,
        }  
      });
      let path =find_image.image;
      

      if (fs.existsSync(path)){
        fs.unlinkSync(path);
        console.log("image Deleted successfully");
      }
      
      
      let update_val ={
        title:json_obj.title,
        content:json_obj.content,
        image: req.file.path,
        user_id:json_obj.user_id
      }

      console.log(update_val);
      
      let driver_home_page_update = await static_driver_homepage.update(update_val, {
        where: { id: json_obj.id }, 
      });

      if (!driver_home_page_update) {
        res.status(200).json({
          status: false,
          message: "home_page update Failed",
        });
        return res;
      }
      if (driver_home_page_update) {
        res.status(200).json({
          status: true,
          message: "home_page update Successfully",
        });
        return res;
      }
    }
  } catch (err) {
    res.status(409).send({
      status: false,
      message: err,
    });
  }
};

const driver_home_page_delete = async (req, res, next) => {
  try {
    let find_image =await static_driver_homepage.findOne({
      where:{
        id:req.body.id,
      }  
    });
    let path =find_image.image;

    if (fs.existsSync(path)){
      fs.unlinkSync(path);
      console.log("image Deleted successfully");
    }

    const delete_driver_home_page = await static_driver_homepage.destroy({
      where: { id: req.body.id },
    });
    console.log("delete id" + req.body.id);
    if (!delete_driver_home_page) {
      res.status(200).json({
        status: false,
        message: "home_page delete failed",
      });
      return res;
    }

    res.status(200).json({
      status: true,
      message: "home_page delete Successfully",
    });
    return res;
  } catch (err) {
    res.status(409).send({
      status: false,
      message:
        err && err.original && err.original.message
          ? err.original.message
          : "Error",
    });
  }
};

module.exports = {
  create,
  read,
  update,
  home_page_delete,
  driver_home_create,
  driver_home_read,
  driver_home_update,
  driver_home_page_delete
};
