const UserModel = require("../../model/drivers/user.model");
let nodemailer = require("nodemailer");
let environment = process.env;
const maillayout =require('../../helper/sendmail.helper');

const approval_Api = async (req, res) => {
  try {

    let getuser = await  UserModel.findOne({
      where:{
          id:req.body.id,
      },
    });

    if(getuser.status == 'approved'){
      res.status(200).json({
        status:false,
        message:'This  User Already Approved'
      })
    }else{
      let updateVal = {
        status: "approved",
    };

    if (req.body.status == "Approval Request") {
      let approvaldata = await UserModel.update(updateVal, {
        where: {
          id: req.body.id,
        },
      });

      if (!approvaldata) {
        res.status(200).json({
          status: true,
          message: "Approval faild",
        });
        return res;
      }

      

      // console.log("reject",getuser.email);
      maillayout.setmail('approval.hbs');
      maillayout.sendEmail(getuser.email,'Driver Registration Details','approval',{ title: 'Title', content: 'Content', user_name:getuser.name }).then((response,error) =>{
        if (!response) {

          res.status(200).json({
              status: false,
              message: 'approval  Mail Sending Failed',
          });
          return res;
      
        } 

      res.status(200).json({
          status: true,
          message: 'Approval Sent To Mail Successfully',
      });

      return res;
      })
    }

    }

    
  } catch (error) {
    console.log(error.message,);
  }
};

const reject_api =async (req,res) =>{
  try{

    let getuser = await  UserModel.findOne({
      where:{
          id:req.body.id,
      },
    });

    if(getuser.status =='reject'){
      res.status(200).json({
        status:false,
        message:'This  User Already Reject'
      })
    }else{
      let updateVal = {
        status: "reject",
      };

      let approvaldata = await UserModel.update(updateVal, {
        where: {
          id: req.body.id,
        },
      });

      if (!approvaldata) {
        res.status(200).json({
          status: true,
          message: "reject faild",
        });
        return res;
      }

      maillayout.setmail('reject.hbs');
      maillayout.sendEmail(getuser.email,'Driver Registration Details','reject',{ title: 'Title', content: 'Content', user_name:getuser.name }).then((response,error) =>{
        if (!response) {
          res.status(200).json({
              status: false,
              message: 'reject  Mail Sending Failed',
          });
          return res;
      
        } 

      res.status(200).json({
          status: true,
          message: 'reject Sent To Mail Successfully',
      });

      return res;
      })

    }

  }catch(error){
    console.log(error.message);
  } 
}

module.exports = {
  approval_Api,
  reject_api
};
