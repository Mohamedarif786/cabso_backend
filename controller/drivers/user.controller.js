const UserModel = require('../../model/drivers/user.model');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const otpGenerator = require('otp-generator');
const FPOtp = require('../../model/fp_otp.model');
const mailer = require('../../helper/sendmail.helper');

const driver_signUp = async(req,res)=>{
    try{

        let json_obj = JSON.parse(req.body.data);

        let emailCheck = await UserModel.findOne({
            where:{
                email:json_obj.email
            }
        })

        if(emailCheck){
            res.status(200).json({
                status:false,
                message:"This Email Already Used"
            })
            return res;
        }

        if (req.files == undefined) {
            return res.send(`You must select a file.`);
        } 
 
         let password_encrypt = await  bcrypt.hash(json_obj.password,10);
         

         let data = {
            name:json_obj.name,
            email:json_obj.email,
            phone:json_obj.phone,
            password:password_encrypt,
            status:"pending",
            license:req.files.licence[0].filename,
            insurance:req.files.insurance[0].filename,
            truck_image:req.files.truck_image[0].filename
         }

         console.log("data",data);
        const Save = await  UserModel.create(data);

        if(!Save){
            res.status(200).json({
                status:false,
                message:"Driver Signup Failed"
            })
            return res;
        }

        if(Save){
            res.status(200).json({
                status:true,
                message:"Driver Signup Successfully"
            })
        }
       
        
    }catch(err){
        console.log(err.message);
    }   
}

const driver_login = async(req,res)=>{
    try{
        const user = await UserModel.findOne({
            where:{
                email:req.body.email
            }
        });
        if(!user){
            res.status(200).json({
                status: false,
                message: 'Invalid Email',
            });
            return res;
        }

        let validPassword = await bcrypt.compare(req.body.password,user.password);

        if(!validPassword){
            res.status(200).json({
                status: false,
                message: 'Invalid Password', 
            });
            return res;
        }

        const userDetail = { id: user.id, email: user.email };
        const jwtToken = process.env.JWT_SECRET;
        const token = jwt.sign({ user: userDetail }, jwtToken);

        res.status(200).json({
            status: true,
            message: 'driver Login Successfully',
            token: token,
            userDetail: userDetail
        });

    }catch(err){
        console.log(err.message);
    }
}

const driver_mailverify = async (req,res)=>{
    try{

        let  user =await  UserModel.findOne({where:{ email:req.body.email}});
        if(!user){
            res.status(200).json({
                status:false,
                message:"Invalid Email"
            })
            return res;
        }
       
       const user_detais ={id:user.id,email:user.email}
       const otp = otpGenerator.generate(6, { digits: true, alphabets: false, upperCase: false, specialChar: false });
       const otp_int = Math.floor(100000 + Math.random() * 900000);
       const otp_expire_at = Date.now() + (5 * 60 * 1000);

       const otp_hashed = await bcrypt.hash(otp.toString(), 10);
       const otp_int_hashed = await bcrypt.hash(otp_int.toString(), 10);

       console.log("otp🚀",otp);

       let user_otp = await FPOtp.findOne({where: { user_id: user.id }});
       let create_otp;

       if (user_otp && user_otp.user_id) {
        create_otp = await FPOtp.update({
            otp: otp_int_hashed,
            otp_expire_at: otp_expire_at
        },
        {
            where: { user_id: user.id }
        });
        
        }
        else {
            create_otp = await FPOtp.create({
                user_id: user.id,
                user_email: user.email,
                otp: otp_int_hashed,
                otp_expire_at: otp_expire_at
            });
        }

        if (!create_otp) {
            res.status(200).json({
                status: false,
                message: 'OTP Creation Failed',
            });
            return res;
        }
    
        mailer.sendEmail(req.body.email, "Forget Password!", "fpotp", { title: 'Title', content: 'Content', otp: otp_int, }).then((response, error) => {
            console.log('response', response);
            if (!response) {
                res.status(200).json({
                    status: false,
                    message: 'OTP Sending Failed',
                });
                return res;
            }
            res.status(200).json({
                status: true,
                message: 'OTP Sent To Mail',
                userDetail: user_detais
            });
            return res;
        });

    }catch(err){
        console.log(err.message);
    }
}

const driver_FPOTPVerify =async (req,res)=>{
    try{
      
        let user_otp = await FPOtp.findOne({
            where: { user_id: req.body.user_id },
        });

        if (!user_otp) {
            res.status(200).json({
                status: false,
                message: 'Invalid User',
            });
            return res;
        }

        const current_at = Date.now();

        const valid_time = current_at < user_otp.otp_expire_at;
        const valid_otp = await bcrypt.compare(req.body.otp, user_otp.otp);

        console.log('valid_time', valid_time);
        console.log('valid_otp', valid_otp);

        if (!valid_time || !valid_otp) {
            if (!valid_time) {
                res.status(200).json({
                    status: false,
                    message: 'Expired OTP Time',
                });
                return res;
            }
            else if (!valid_otp) {
                res.status(200).json({
                    status: false,
                    message: 'Invalid OTP',
                });
                return res;
            }
        }

        const userDetail = { user_id: user_otp.user_id, user_email: user_otp.user_email };
        const jwtToken = process.env.JWT_SECRET;
        const token = jwt.sign({ user: userDetail }, jwtToken);

        res.status(200).json({
            status: true,
            message: 'OTP Verified Successfully',
            token: token,
            userDetail: userDetail
        });
        return res;
    }catch(err){
        console.log(err.message);
    }
}

const driver_FPResetPassword = async (req,res)=>{
    try{
        req.body.reset_password = await bcrypt.hash(req.body.reset_password, 10);
        let reset_pass = await UserModel.update({
            password: req.body.reset_password
        },
        {
            where: { id: req.body.user_id }
        });
        console.log("update Value"+reset_pass); 

        if (!reset_pass) {
            res.status(200).json({
                status: false,
                message: 'Password Resetting Failed',
            });
            return res;
        }

        res.status(200).json({
            status: true,
            message: 'Password Resetted Successfully',
        });
        return res;
    }catch(err){
        console.log(err.message);
    }
}

const gellAll_driver = async (req,res)=>{
    try{
        
        let getUser = await UserModel.findAll();

        if(!getUser){
            res.status(200).json({
                status:false,
                Message:"get all Pending Drivers Faild"            
            })

            return res;
        }

        res.status(200).json({
            status:true,
            Message:"get all Pending Drivers Successfully",
            data:getUser       
        });

    }catch(err){
        console.log(err.message);
    }
} 

module.exports = {
    driver_signUp,
    driver_login,
    driver_mailverify,
    driver_FPOTPVerify,
    driver_FPResetPassword,
    gellAll_driver
}