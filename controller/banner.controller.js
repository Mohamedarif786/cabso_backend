const banner_modal =require('../model/banner.model');
const fs =require("fs");

const create =async (req,res)=>{
    try{
        console.log("data" , JSON.parse(req.body.data));
        let json_obj = JSON.parse(req.body.data);
        console.log("image" , req.file.path);

        if (req.file == undefined) {
            return res.send(`You must select a file.`);
        }

        let data ={
            title:json_obj.title,
            image: req.file.path,
            user_id:json_obj.user_id
        } 
        let banner_create = await banner_modal.create(data);

        if (!banner_create) {
            res.status(200).json({
              status: false,
              message: "Banner Creation Failed",
            });
            return res;
        }

        res.status(200).json({
            status: true,
            message: "Banner Created Successfully",
        });

        return res;

    }catch(err){
        console.log(err.message);
    }
}
const read =async (req,res)=>{
    try{
        
        let banner_getall = await banner_modal.findAll();
         
        if (!banner_getall) {
            res.status(200).json({
              status: false,
              message: "banner get Failed",
            });
            return res;
        }

        res.status(200).json({
            status:true,
            message:"Banner Get Successfully",
            data:banner_getall
        })
        return res;
    }catch(err){
        console.log(err.message);
    }
}

const update =async (req,res)=>{
    try{
        let json_obj = JSON.parse(req.body.data);
        
        if (req.file == undefined) {
            // return res.send(`You must select a file.`);
            let update_val ={
              title:json_obj.title,
              user_id:json_obj.user_id
            }
            
            let home_page_update = await banner_modal.update(update_val, {
              where: { id: json_obj.id }, 
            });
      
      
            if (!home_page_update) {
              res.status(200).json({
                status: false,
                message: "Banner Update Failed",
              });
              return res;
            }
            if (home_page_update) {
              res.status(200).json({
                status: true,
                message: "Banner Update Successfully",
              });
              return res;
            }
            
          }else{
            let find_image =await banner_modal.findOne({
              where:{
                id:json_obj.id,
              }  
            });
            let path =find_image.image;
        
            if (fs.existsSync(path)){
              fs.unlinkSync(path);
              console.log("Banner Deleted Successfully");
            }
            
            let update_val ={
              title:json_obj.title,
              image: req.file.path,
              user_id:json_obj.user_id
            }
        
            console.log(update_val);
            
            let home_page_update = await home_page_modal.update(update_val, {
              where: { id: json_obj.id }, 
            });
      
      
            if (!home_page_update) {
              res.status(200).json({
                status: false,
                message: "Banner Update Failed",
              });
              return res;
            }
            if (home_page_update) {
              res.status(200).json({
                status: true,
                message: "Banner Update Successfully",
              });
              return res;
            }
          }
    }catch(err){
        console.log(err.message);
    }
}

const banner_delete =async(req,res)=>{
    try{
        
        let find_image =await banner_modal.findOne({
            where:{
              id:req.body.id,
            }  
        });

        let path =find_image.image;

        if (fs.existsSync(path)){
        fs.unlinkSync(path);
        console.log("image Deleted successfully");
        }

        let delete_banner =await banner_modal.destroy({
            where:{
                id:req.body.id
            }
        });

        if (!delete_banner) {
            res.status(200).json({
              status: false,
              message: "Banner delete failed",
            });
            return res;
        }

        res.status(200).json({
            status: true,
            message: "Banner delete Successfully",
        });
        return res;

    }catch(err){
        console.log(err.message);
    }
}
module.exports = {
    create,
    read,
    update,
    banner_delete
}