const admin_ton =require('../../model/admin_ton.model');


const save = async (req,res)=>{
    try{
       console.log(req.body);
            let data ={
                qty:req.body.qty,
                price:req.body.price,
                user_id:req.body.user_id
            }
        const store =await admin_ton.create(data); 

        if(!store){
            res.status(200).json({
                status:false,
                message:'Admin Price Settings Store Failed'
            })
            return res;
        }else{
            res.status(200).json({
                status:true,
                message:'Admin Price Settings Store Successfully'
            })
            return res;
        }
        
    }catch(error){
        console.log(error.message);
    }
}

const getall = async (req,res)=>{
    try{
        const getall = await admin_ton.findAll();

        if(!getall){
            res.status(200).json({
                status:false,
                message:'Admin Price Settings Getall Failed'
            })
            return res;
        }else{
            res.status(200).json({
                status:true,
                message:'Admin Price Settings Getall Successfully',
                data:getall
            })
            return res;
        }
    }catch(error){

    }
}

const update = async (req,res) =>{
    try{
        console.log(req.body);
             let data ={
                 qty:req.body.qty,
                 price:req.body.price,
                 user_id:req.body.user_id
             }
         const store =await admin_ton.update(data,{
            where:{
                id:req.body.id
            }
         }); 
 
         if(!update){
             res.status(200).json({
                 status:false,
                 message:'Admin Price Settings Update Failed'
             })
             return res;
         }else{
             res.status(200).json({
                 status:true,
                 message:'Admin Price Settings Update Successfully'
             })
             return res;
         }
         
     }catch(error){
         console.log(error.message);
     }
}

module.exports = {
    save,
    getall,
    update
}