const body_type = require('../model/truck_models/body_type.js');
const amenities =require("../model/truck_models/amenities.model");
const category =require("../model/truck_models/category.model");
const fs =require("fs");

// bodyType
const save = async (req, res) => {
  try {

    let data = {
      user_id: req.body.user_id,
      body_type: req.body.body_type
    }

    const bodyTypeSave = await body_type.create(data);

    if (!bodyTypeSave) {
      res.status(200).json({
        status: false,
        message: "bodyTypeSave Creation Failed",
      });
      return res;
    }

    res.status(200).json({
      status: true,
      message: "bodyTypeSave Created Successfully",
    });

    return res;
  } catch (err) {
    console.log("err", err);
  }
}

const getall_data = async (req, res) => {
  try {
    const getall_data = await body_type.findAll();
    console.log("data", getall_data);
    if (!getall_data) {
      res.status(200).json({
        status: false,
        message: "getall_data get Failed",
      });
      return res;
    }

    res.status(200).json({
      status: true,
      message: "getall_data get Successfully",
      data: getall_data,
    });
    return res;

  } catch (err) {
    console.log("err", err);
  }
}

const update_data = async (req, res) => {
  try {
    console.log("value", req.body);
    const bodyType_update = body_type.update({
      body_type: req.body.bodyType,
    }, {
      where: {
        id: req.body.id
      }
    })

    if (!bodyType_update) {
      res.status(200).json({
        status: false,
        message: "bodyType Update Failed",
      });
      return res;
    }
    
    res.status(200).json({
      status: true,
      message: "bodyType Update Successfully"
    });
    return res;


  } catch (err) {
    console.log(err.message);
  }
}

const  delete_data= async(req,res)=>{
  try{
    console.log(req.body.id);

    const delete_data =await body_type.destroy({
      where: {
        id: req.body.id
      }
    });
     
    if (!delete_data) {
      res.status(200).json({
        status: false,
        message: "bodyType Delete Failed",
      });
      return res;
    }
    
    res.status(200).json({
      status: true,
      message: "bodyType Delete Successfully"
    });
    return res;

  }catch(err){
    console.log(err.message);
  }
}

// amenities
const amenities_save = async(req,res)=>{
  try{
    let json_obj = JSON.parse(req.body.data);
    console.log("json_obj",json_obj);
    console.log("image🚀",req.file.path)
    if (req.file == undefined) {
      return res.send(`You must select a file.`);
    } 

    let data ={
      title:json_obj.title,
      Image: req.file.path,
      user_id:json_obj.user_id
    }
    console.log("data",data);
    let amenities_Save = await amenities.create(data);
    console.log("amenities_save 😡",amenities_save);

    if(!amenities_Save){
      res.status(200).json({
        status:false,
        message:"Amenities Create faild"
      })
      return res;
    }

    res.status(200).json({
      status:true,
      message:"Amenities Created Successfully"
    })
    return res;

  }catch(err){
    console.log(err.message);
  }
}

const amenities_getall = async (req,res)=>{
  try{
    let amenities_getdata = await amenities.findAll();

    if(!amenities_getdata){
      res.status(200).json({
        status:false,
        message:"Amenities GetallData Failed"
      })
      return res;
    }

    res.status(200).json({
      status:true,
      message:"Amenities GetallData Successfully",
      data:amenities_getdata
    })
    return res;
  }catch(err){
    console.log(err.message);
  }
}

const amenities_update = async (req,res)=>{
  try{
    let json_obj = JSON.parse(req.body.data);

    if(req.file == undefined){
      
      let update_val ={
        title:json_obj.title,
        user_id:json_obj.user_id
      }

      let amenities_update = await amenities.update(update_val, {
        where: { id: json_obj.id }, 
      });

      if (!amenities_update) {
        res.status(200).json({
          status: false,
          message: "Amenities Update Failed",
        });
        return res;
      }
      if (amenities_update) {
        res.status(200).json({
          status: true,
          message: "Amenities Update Successfully",
        });
        return res;
      }

    }else{
        let find_image =await amenities.findOne({
          where:{
            id:json_obj.id,
          }  
        });

        let path =find_image.image;

        if (fs.existsSync(path)){
          fs.unlinkSync(path);
          console.log("image Deleted successfully");
        }

        let update_val ={
          title:json_obj.title,
          Image: req.file.path,
          user_id:json_obj.user_id
        }

        let amenities_update = await amenities.update(update_val, {
          where: { id: json_obj.id }, 
        });

        if (!amenities_update) {
          res.status(200).json({
            status: false,
            message: "Amenities Update Failed",
          });
          return res;
        }
        if (amenities_update) {
          res.status(200).json({
            status: true,
            message: "Amenities Update Successfully",
          });
          return res;
        }
      }    
        
  }catch(err){
    console.log(err.message);
  }
}

const amenities_delete =async (req,res)=>{
  try{
    let find_image =await amenities.findOne({
      where:{
        id:req.body.id,
      }  
    });
    let path =find_image.image;

    if (fs.existsSync(path)){
      fs.unlinkSync(path);
      console.log("image Deleted successfully");
    }

    const amenities_deletedata = await amenities.destroy({
      where: { id: req.body.id },
    });

    if (!amenities_deletedata) {
      res.status(200).json({
        status: false,
        message: "Amenities Delete failed",
      });
      return res;
    }

    res.status(200).json({
      status: true,
      message: "Amenities Delete Successfully",
    });
    return res;

  }catch(err){
    console.log(err.message);
  }
}
// category
const category_save = async (req, res) => {
  try {

    console.log("data" , JSON.parse(req.body.data));
    let json_obj = JSON.parse(req.body.data);
    console.log("image" , req.file.path);

    if (req.file == undefined) {
      return res.send(`You must select a file.`);
    } 

    let data = {
      user_id:json_obj.user_id,
      category_name:json_obj.category_name,
      category_description:json_obj.category_description,
      body_type:json_obj.body_type,
      amenities:json_obj.amenities,
      unit_price:json_obj.unit_price,
      base_price:json_obj.base_price,
      category_image:req.file.path
    }

    const category_Save = await category.create(data);

    if (!category_Save) {
      res.status(200).json({
        status: false,
        message: "Category Creation Failed",
      });
      return res;
    }

    res.status(200).json({
      status: true,
      message: "Category Created Successfully",
    });

    return res;
  } catch (err) {
    console.log("err", err.message);
  }
}

const category_getall = async (req,res)=>{
  try{
    let category_getdata =category.findAll();

    if(!category_getdata){
      res.status(200).json({
        status:false,
        message:"Category GetallData Failed"
      })
      return res;
    }

    res.status(200).json({
      status:true,
      message:"Category GetallData Successfully",
      data:category_getdata
    })
    return res;
  }catch(err){
    console.log(err.message);
  }
}

const category_update = async (req,res)=>{
  try{
    let json_obj = JSON.parse(req.body.data);

    if(req.file ==undefined){
      let update_val ={
        user_id:json_obj.user_id,
        category_name:json_obj.category_name,
        category_description:json_obj.category_description,
        body_type:json_obj.body_type,
        amenities:json_obj.amenities,
        unit_price:json_obj.unit_price,
        base_price:json_obj.base_price
      }

      let category_Update = await category.update(update_val, {
        where: { id: json_obj.id }, 
      });

      if (!category_Update) {
        res.status(200).json({
          status: false,
          message: "Category Update Failed",
        });
        return res;
      }
      if (category_Update) {
        res.status(200).json({
          status: true,
          message: "Category Update Successfully",
        });
        return res;
      }

    }else{

        let find_image =await category.findOne({
          where:{
            id:json_obj.id,
          }  
        });

        let path =find_image.image;

        if (fs.existsSync(path)){
          fs.unlinkSync(path);
          console.log("image Deleted successfully");
        }

        let update_val ={
          user_id:json_obj.user_id,
          category_name:json_obj.category_name,
          category_description:json_obj.category_description,
          body_type:json_obj.body_type,
          amenities:json_obj.amenities,
          unit_price:json_obj.unit_price,
          base_price:json_obj.base_price,
          category_image:req.file.path
        }

        let category_Update = await category.update(update_val, {
          where: { id: json_obj.id }, 
        });

        if (!category_Update) {
          res.status(200).json({
            status: false,
            message: "Category Update Failed",
          });
          return res;
        }
        if (category_Update) {
          res.status(200).json({
            status: true,
            message: "Category Update Successfully",
          });
          return res;
        }
    }
  }catch(err){
    console.log(err.message);
  }
}

const category_delete =async (req,res)=>{
  try{
    let find_image =await category.findOne({
      where:{
        id:req.body.id,
      }  
    });
    let path =find_image.image;

    if (fs.existsSync(path)){
      fs.unlinkSync(path);
      console.log("image Deleted successfully");
    }

    const category_deletedata = await category.destroy({
      where: { id: req.body.id },
    });

    if (!category_deletedata) {
      res.status(200).json({
        status: false,
        message: "Category Delete failed",
      });
      return res;
    }

    res.status(200).json({
      status: true,
      message: "Category Delete Successfully",
    });
    return res;

  }catch(err){
    console.log(err.message);
  }
}


module.exports = {
  save,
  getall_data,
  update_data,
  delete_data,
  amenities_save,
  amenities_getall,
  amenities_update,
  amenities_delete,
  category_save,
  category_getall,
  category_update,
  category_delete
}