const UserModel = require('../../model/riders/user.model');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const otpGenerator = require('otp-generator');
const FPOtp = require('../../model/fp_otp.model');
const mailer = require('../../helper/sendmail.helper');


const rider_signUp = async(req,res)=>{
    try{
        req.body.password = await bcrypt.hash(req.body.password, 10);
        const Save = await  UserModel.create(req.body);

        if(!Save){
            res.status(200).json({
                status:false,
                message:"Rider Signup Failed"
            })
            return res;
        }

        res.status(200).json({
            status:true,
            message:"Rider Signup Successfully"
        })
        
    }catch(err){
        console.log(err.message);
    }   
}

const rider_login = async (req,res) =>{
    try {
        let user = await UserModel.findOne({
            where:{
                email:req.body.email
            }
        });
        
        if (!user) {
            res.status(200).json({
                status: false,
                message: 'Invalid Email',
            });
            return res;
        } 
        
        let validPassword = await bcrypt.compare(req.body.password,user.password);

        if (!validPassword) {
            res.status(200).json({
                status: false,
                message: 'Invalid Password', 
            });
            return res;
        }
         
        const userDetail = { id: user.id, email: user.email };
        const jwtToken = process.env.JWT_SECRET;
        const token = jwt.sign({ user: userDetail }, jwtToken);

        res.status(200).json({
            status: true,
            message: 'Riders Login Successfully',
            token: token,
            userDetail: userDetail
        });

    }catch(err){
        console.log(err.message);
    }
   
}

const rider_mailverify = async (req,res)=>{
    try{
       let  user =await  UserModel.findOne({where:{ email:req.body.email}});
       if(!user){
         res.status(200).json({
            status:false,
            message:"Invalid Email"
         })
         return res;
       }
       
       const user_detais ={id:user.id,email:user.email}
       const otp = otpGenerator.generate(6, { digits: true, alphabets: false, upperCase: false, specialChar: false });
       const otp_int = Math.floor(100000 + Math.random() * 900000);
       const otp_expire_at = Date.now() + (5 * 60 * 1000);


       const otp_hashed = await bcrypt.hash(otp.toString(), 10);
       const otp_int_hashed = await bcrypt.hash(otp_int.toString(), 10);

       console.log("otp🚀",otp);

       let user_otp = await FPOtp.findOne({where: { user_id: user.id }});
       let create_otp;

       if (user_otp && user_otp.user_id) {
            create_otp = await FPOtp.update({
                otp: otp_int_hashed,
                otp_expire_at: otp_expire_at
            },
            {
                where: { user_id: user.id }
            });
        }
        else {
            create_otp = await FPOtp.create({
                user_id: user.id,
                user_email: user.email,
                otp: otp_int_hashed,
                otp_expire_at: otp_expire_at
            });
        }

        if (!create_otp) {
            res.status(200).json({
                status: false,
                message: 'OTP Creation Failed',
            });
            return res;
        }
        
        mailer.sendEmail(req.body.email, "Forget Password!", "fpotp", { title: 'Title', content: 'Content', otp: otp_int, }).then((response, error) => {
            console.log('response', response);
            if (!response) {
                res.status(200).json({
                    status: false,
                    message: 'OTP Sending Failed',
                });
                return res;
            }
            res.status(200).json({
                status: true,
                message: 'OTP Sent To Mail',
                userDetail: user_detais
            });
            return res;
        });


    }catch(err){
        console.log(err.message);
    }
}

const rider_FPOTPVerify =async (req,res)=>{
    try{
      
        let user_otp = await FPOtp.findOne({
            where: { user_id: req.body.user_id },
        });

        if (!user_otp) {
            res.status(200).json({
                status: false,
                message: 'Invalid User',
            });
            return res;
        }

        const current_at = Date.now();

        const valid_time = current_at < user_otp.otp_expire_at;
        const valid_otp = await bcrypt.compare(req.body.otp, user_otp.otp);

        console.log('valid_time', valid_time);
        console.log('valid_otp', valid_otp);

        if (!valid_time || !valid_otp) {
            if (!valid_time) {
                res.status(200).json({
                    status: false,
                    message: 'Expired OTP Time',
                });
                return res;
            }
            else if (!valid_otp) {
                res.status(200).json({
                    status: false,
                    message: 'Invalid OTP',
                });
                return res;
            }
        }

        const userDetail = { user_id: user_otp.user_id, user_email: user_otp.user_email };
        const jwtToken = process.env.JWT_SECRET;
        const token = jwt.sign({ user: userDetail }, jwtToken);

        res.status(200).json({
            status: true,
            message: 'OTP Verified Successfully',
            token: token,
            userDetail: userDetail
        });
        return res;
    }catch(err){
        console.log(err.message);
    }
}

const rider_FPResetPassword  = async (req ,res)=>{
    try{
        req.body.reset_password = await bcrypt.hash(req.body.reset_password, 10);
        let reset_pass = await UserModel.update({
            password: req.body.reset_password
        },
        {
            where: { id: req.body.user_id }
        });
        console.log("update Value"+reset_pass); 

        if (!reset_pass) {
            res.status(200).json({
                status: false,
                message: 'Password Resetting Failed',
            });
            return res;
        }

        res.status(200).json({
            status: true,
            message: 'Password Resetted Successfully',
        });
        return res;
    }catch(err){
        console.log(err.message);
    }
}

const get_riders =async (req,res)=>{
    try{
        
        let getUser = await UserModel.findAll();
        console.log("hi");

        if(!getUser){
            res.status(200).json({
                status:false,
                Message:"get all Riders Faild"            
            })

            return res;
        }

        res.status(200).json({
            status:true,
            Message:"get all Riders Successfully",
            data:getUser          
        });

    }catch(err){
        console.log(err.message);
    }
}

module.exports={
    rider_signUp,
    rider_login,
    rider_mailverify,
    rider_FPOTPVerify,
    rider_FPResetPassword,
    get_riders
}