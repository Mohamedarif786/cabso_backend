const user_request = require('../../model/riders/user_request.model');

const request_store = async (req,res)=>{
    try{
        // console.log("value",req.body);
        let data = {
            name:req.body.name,
            Qty:req.body.qty,
            price:req.body.price,
            user_id:req.body.user_id,
            status:'pending'
        }
        const request_store = await user_request.create(data);
        
        if(request_store){
            res.status(200).json({
                status:true,
                message:'Request Store Successfully'
            })
        }else{
            res.status(200).json({
                status:false,
                message:'Request Store Faield'
            })
        }

    }catch(error){
        console.log(error.message);
    }
}

const requestall = async (req,res)=>{
    try{
       
       const  requestgetall = await user_request.findAll();

       if(!requestgetall){
        res.status(200).json({
            status:false,
            message:'User All Request Get Failed'
        })
        return res;
       }

       res.status(200).json({
        status:true,
        message:"User All Request Get Successfully",
        data:requestgetall
       })

    }catch(error){
        console.log(error.message);
    }
}

const request_approval = async (req,res)=>{
    try{
        if(req.body.status == 'approval'){
            let user = await user_request.findOne({
                where:{
                    id:req.body.id
                }
            })

            if(user.status =='approval'){
                res.status(200).json({
                    status:false,
                    message:'This User Already Approved'
                })
            }else{

                let data = {
                    status:'approval'
                }

               let approval = await user_request.update(data,{
                  where:{
                    id:req.body.id
                  }
               });

               if(!approval){
                 res.status(500).json({
                    status:false,
                    message:'This User Approved Failed'
                 })
                 return res;
               }

               res.status(200).json({
                status:true,
                message:'This User Approved Successfully'
               })
            }

        }
    }catch(error){
        console.log(error.message);
    }
}

const request_reject = async (req,res) =>{
    try{
        if(req.body.status == 'reject'){
            let user = await user_request.findOne({
                where:{
                    id:req.body.id
                }
            })

            if(user.status =='reject'){
                res.status(200).json({
                    status:false,
                    message:'This User Already Rejected'
                })
            }else{

                let data = {
                    status:'reject'
                }

               let reject = await user_request.update(data,{
                  where:{
                    id:req.body.id
                  }
               });

               if(!reject){
                 res.status(500).json({
                    status:false,
                    message:'This User Reject Failed'
                 })
                 return res;
               }

               res.status(200).json({
                status:true,
                message:'This User Reject Successfully'
               })
            }

        }
    }catch(error){
        console.log(error.message);
    }
}

module.exports = {
    request_store,
    requestall,
    request_approval,
    request_reject
}