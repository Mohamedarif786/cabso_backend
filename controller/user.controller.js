const jwt = require('jsonwebtoken');
const moment = require('moment');
const sequelize = require('sequelize');
const pp = sequelize.Op;
const mailer = require('../helper/sendmail.helper');

const UserModel = require('../model/users.model');
const FPOtp = require('../model/fp_otp.model');

const bcrypt = require('bcrypt');
const otpGenerator = require('otp-generator');

const { where } = require('sequelize');
const { QueryTypes } = require('sequelize');

const db = require('../config/db.config');


const SignUp = async (req, res, next) => {
    try { 
        req.body.password = await bcrypt.hash(req.body.password, 10);

        let user = await UserModel.create(req.body);

        if (!user) {
            res.status(200).json({
                status: false,
                message: 'User Creation Failed',
            });
            return res;
        }

        res.status(200).json({
            status: true,
            message: 'User Created Successfully',
        });
        return res;
    }
    catch (err) {
        res.status(409).send({ status: false, message: err && err.original && err.original.message ? err.original.message : 'Error' });
    }
}

const Login = async (req, res, next) => {
    try {
        console.log("login"+req.body.email);
        let user = await UserModel.findOne({
            where: { email: req.body.email },
        });

        if (!user) {
            res.status(200).json({
                status: false,
                message: 'Invalid Email',
            });
            return res;
        }

        const validPassword = await bcrypt.compare(req.body.password, user.password);

        if (!validPassword) {
            res.status(200).json({
                status: false,
                message: 'Invalid Password',
            });
            return res;
        }

        const userDetail = { id: user.id, email: user.email };
        const jwtToken = process.env.JWT_SECRET;
        const token = jwt.sign({ user: userDetail }, jwtToken);

        res.status(200).json({
            status: true,
            message: 'Login Successfully',
            token: token,
            userDetail: userDetail
        });
        return res;
    }
    catch (err) {
        res.status(409).send({ status: false, message: err && err.original && err.original.message ? err.original.message : 'Error' });
    }
}

const FPMailVerify = async (req, res, next) => {
    try {
        let user = await UserModel.findOne({
            where: { email: req.body.email },
        });

        if (!user) {
            res.status(200).json({
                status: false,
                message: 'Invalid Email',
            });
            return res;
        }

        const userDetail = { id: user.id, email: user.email };

        const otp = otpGenerator.generate(6, { digits: true, alphabets: false, upperCase: false, specialChar: false });
        const otp_int = Math.floor(100000 + Math.random() * 900000);
        const otp_expire_at = Date.now() + (5 * 60 * 1000);

        const otp_hashed = await bcrypt.hash(otp.toString(), 10);
        const otp_int_hashed = await bcrypt.hash(otp_int.toString(), 10);

        console.log('otp', otp);
        console.log('otp_int', otp_int);
        console.log('otp_expire_at', otp_expire_at);

        console.log('otp_hashed', otp_hashed);
        console.log('otp_int_hashed', otp_int_hashed);


        let user_otp = await FPOtp.findOne({
            where: { user_id: user.id },
        });

        let create_otp;
        if (user_otp && user_otp.user_id) {
            create_otp = await FPOtp.update({
                otp: otp_int_hashed,
                otp_expire_at: otp_expire_at
            },
            {
                where: { user_id: user.id }
            });
        }
        else {
            create_otp = await FPOtp.create({
                user_id: user.id,
                user_email: user.email,
                otp: otp_int_hashed,
                otp_expire_at: otp_expire_at
            });
        }

        if (!create_otp) {
            res.status(200).json({
                status: false,
                message: 'OTP Creation Failed',
            });
            return res;
        }

        mailer.sendEmail(req.body.email, "Forget Password!", "fpotp", { title: 'Title', content: 'Content', otp: otp_int, }).then((response, error) => {
            console.log('response', response);
            if (!response) {
                res.status(200).json({
                    status: false,
                    message: 'OTP Sending Failed',
                });
                return res;
            }
            res.status(200).json({
                status: true,
                message: 'OTP Sent To Mail',
                userDetail: userDetail
            });
            return res;
        });
    }
    catch (err) {
        res.status(409).send({ status: false, message: err && err.original && err.original.message ? err.original.message : 'Error' });
    }
}

const FPOTPVerify = async (req, res, next) => {
    try {
        let user_otp = await FPOtp.findOne({
            where: { user_id: req.body.user_id },
        });

        if (!user_otp) {
            res.status(200).json({
                status: false,
                message: 'Invalid User',
            });
            return res;
        }

        const current_at = Date.now();

        const valid_time = current_at < user_otp.otp_expire_at;
        const valid_otp = await bcrypt.compare(req.body.otp, user_otp.otp);

        console.log('valid_time', valid_time);
        console.log('valid_otp', valid_otp);

        if (!valid_time || !valid_otp) {
            if (!valid_time) {
                res.status(200).json({
                    status: false,
                    message: 'Expired OTP Time',
                });
                return res;
            }
            else if (!valid_otp) {
                res.status(200).json({
                    status: false,
                    message: 'Invalid OTP',
                });
                return res;
            }
        }

        const userDetail = { user_id: user_otp.user_id, user_email: user_otp.user_email };
        const jwtToken = process.env.JWT_SECRET;
        const token = jwt.sign({ user: userDetail }, jwtToken);

        res.status(200).json({
            status: true,
            message: 'OTP Verified Successfully',
            token: token,
            userDetail: userDetail
        });
        return res;
    }
    catch (err) {
        res.status(409).send({ status: false, message: err && err.original && err.original.message ? err.original.message : 'Error' });
    }
}

const FPResetPassword = async (req, res, next) => {
    try {
        req.body.reset_password = await bcrypt.hash(req.body.reset_password, 10);

        let reset_pass = await UserModel.update({
            password: req.body.reset_password
        },
        {
            where: { id: req.body.user_id }
        });
        console.log("update Value"+reset_pass); 

        if (!reset_pass) {
            res.status(200).json({
                status: false,
                message: 'Password Resetting Failed',
            });
            return res;
        }

        res.status(200).json({
            status: true,
            message: 'Password Resetted Successfully',
        });
        return res;
    }
    catch (err) {
        res.status(409).send({ status: false, message: err && err.original && err.original.message ? err.original.message : 'Error' });
    }
}

const UserDetails = async (req, res, next) => {
    try {
        let UserDetails = await UserModel.findAll();

        res.status(200).json({
            status: true,
            message: 'UserDetails',
            UserDetails: UserDetails
        });
        return res;
    }
    catch (err) {
        res.status(409).send({ status: false, message: err && err.original && err.original.message ? err.original.message : 'Error' });
    }
}

const FindUserMail = async (req, res, next) => {
    try {
        let userMail = await UserModel.findAll({
            where: { email: req.body.email },
        });

        res.status(200).json({
            status: true,
            message: userMail && userMail.length ? 'Email Already Exist' : 'Email Does Not Exist',
            UserDetails: userMail && userMail.length ? userMail : []
        });
        return res;
    }
    catch (err) {
        res.status(409).send({ status: false, message: err && err.original && err.original.message ? err.original.message : 'Error' });
    }
}

module.exports = { SignUp, Login, FPMailVerify, FPOTPVerify, FPResetPassword, UserDetails, FindUserMail };
