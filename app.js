const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const mongoose = require('mongoose')
const socket = require('socket.io')
const jwt = require('jsonwebtoken')
const multer  = require('multer')
dotenv.config();

const app = express();

const usersRouter = require('./routes/users.route');
const static_page = require('./routes/staticpage.route');
const truckRouter =require('./routes/truck.route');
const bannerRouter =require('./routes/banner.route');
const riderRouter =require('./routes/riders/users.route');
const driverRouter =require('./routes/drivers/users.route');
const adminRouter =require('./routes/admin/setting.route')

// const authRoute =require('./routes/auth.route');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use('/public/images/',express.static(path.join(__dirname, 'public/images/')));
app.use(express());
app.use(bodyParser.urlencoded({
  extended:true
}));
app.use(cors());


app.use('/api/users', usersRouter);
app.use('/api/static_page', static_page);
app.use('/api/trucks',truckRouter);
app.use('/api/banner',bannerRouter);
app.use('/api/admin',adminRouter)
app.use('/api/rider',riderRouter);
app.use('/api/driver',driverRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(3000,()=>{
  console.log('server connected on port 3000')
})

module.exports = app;
