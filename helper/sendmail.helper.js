let nodemailer = require("nodemailer");
let hbs = require("nodemailer-express-handlebars");
const { resolve } = require("path");
const { nextTick } = require("process");
let environment = process.env;

const SMTPTransport = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: environment.SMTP_USER_NAME,
    pass: environment.SMTP_USER_PASSWORD,
  },
  tls: {
    rejectUnauthorized: false,
  },
});

const options = {
  viewEngine: {
    extname: ".hbs",
    partialsDir: __dirname + "/views/email/partials/",
    layoutsDir: __dirname + "/views/email/",
    defaultLayout: "",
  },
  viewPath: __dirname + "/views/email/",
  extName: ".hbs",
};

module.exports.setmail = (dirname) => {
  try {
    options.viewEngine.defaultLayout = dirname;
  } catch (error) {}
};

SMTPTransport.use("compile", hbs(options));

module.exports.sendEmail = (to, subject, template, data) => {
  return new Promise((resolve, reject) => {
    console.log("to",to);
    console.log("subject",subject);
    console.log("template",template);
    console.log("data",data);
    let mailOptions = {
      from: environment.SMTP_USER_NAME,
      to: to,
      subject: subject,
      template: template,
      context: data,
    };
    try {
      SMTPTransport.sendMail(mailOptions, (error, info) => {
        if (error) {
          console.log("error mail send", error);
          resolve(false);
        } else {
          console.log("email is send");
          resolve(true);
        }
      });
    } catch (e) {
      console.log(e);
      resolve(false);
    }
  });

  // return true;
};
