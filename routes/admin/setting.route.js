const  express = require('express');
const router = express.Router();
const settingcontroller = require('../../controller/admin/setting.controller');

// setting
router.post('/settings/price-settings/save',settingcontroller.save);
router.get('/settings/price-settings/getall',settingcontroller.getall);
router.post('/settings/price-settings/update',settingcontroller.update);

module.exports = router;