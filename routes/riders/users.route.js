
const user_controller =require('../../controller/riders/user.controller');
const user_request_controller =require('../../controller/riders/user-request.controller');

const verify =require('../../middleware/auth.middleware');

var express = require('express');
var router = express.Router();

router.post('/signup',user_controller.rider_signUp);
router.post('/login',user_controller.rider_login);
router.post('/fp-mail-verify',user_controller.rider_mailverify);
router.post('/fp-otp-verify',user_controller.rider_FPOTPVerify);
router.post('/fp-reset-password',verify,user_controller.rider_FPResetPassword);
router.get('/get-riders',user_controller.get_riders);

// user_request
router.post('/req-store',user_request_controller.request_store);
router.get('/req-getall',user_request_controller.requestall);
router.post('/req-approval',user_request_controller.request_approval);
router.post('/req-reject',user_request_controller.request_reject);



module.exports = router;