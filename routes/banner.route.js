const  express = require('express');
const router = express.Router();
const banner_controller =require('../controller/banner.controller');
const upload = require("../middleware/upload");
  
router.post('/create',upload.single('image'),banner_controller.create);
router.get('/read',banner_controller.read);
router.post('/update',upload.single('image'),banner_controller.update);
router.post('/delete',banner_controller.banner_delete);
module.exports = router;