const user_controller =require('../../controller/drivers/user.controller');
const approvedController =require('../../controller/drivers/driver-approval.controller');

const verify =require('../../middleware/auth.middleware');
const upload = require("../../middleware/upload");

var express = require('express');
var router = express.Router();

router.post('/signup',upload.fields([{ name:'licence',maxCount:1},{name:"insurance",maxCount:1},{name:"truck_image",maxCount:1}]),user_controller.driver_signUp);
router.post('/login',user_controller.driver_login);
router.post('/fp-mail-verify',user_controller.driver_mailverify);
router.post('/fp-otp-verify',user_controller.driver_FPOTPVerify);
router.post('/fp-reset-password',verify,user_controller.driver_FPResetPassword);
router.get('/get-drivers',user_controller.gellAll_driver);



// approval api 

router.post('/approved',approvedController.approval_Api);
router.post('/reject',approvedController.reject_api);

module.exports =router;
