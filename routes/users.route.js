// var express = require('express');
// var router = express.Router();

// /* GET users listing. */
// router.get('/', function(req, res, next) {
//   res.send('respond with a resource');
// });

// module.exports = router;

const verify = require('../middleware/auth.middleware')

var express = require('express');
var router = express.Router();

const UserController = require("../controller/user.controller");

router.post('/login', UserController.Login);
router.post('/signup', UserController.SignUp);

router.post('/fp-mail-verify', UserController.FPMailVerify);
router.post('/fp-otp-verify', UserController.FPOTPVerify);
router.post('/fp-reset-password', verify, UserController.FPResetPassword);

router.post('/find-mail', verify, UserController.FindUserMail);

router.get('/user-details', verify, UserController.UserDetails);

module.exports = router;
