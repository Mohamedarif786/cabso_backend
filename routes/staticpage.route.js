
const verify = require('../middleware/auth.middleware');

const static_controller =require('../controller/staticpages.controller')

const upload = require("../middleware/upload");
var express = require('express');
var router = express.Router();

router.post('/create',upload.single("image"), static_controller.create);
router.get('/read', static_controller.read);
router.post('/update',upload.single("image"),static_controller.update);
router.post('/delete', static_controller.home_page_delete);

router.post('/driver_homepage/create',upload.single("image"), static_controller.driver_home_create);
router.get('/driver_homepage/read', static_controller.driver_home_read);
router.post('/driver_homepage/update',upload.single("image"),static_controller.driver_home_update);
router.post('/driver_homepage/delete', static_controller.driver_home_page_delete);


module.exports = router;
