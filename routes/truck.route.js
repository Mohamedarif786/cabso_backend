const express =require('express');
const router =express.Router();
const truckController =require('../controller/trucks.controller.js');
const upload =require('../middleware/upload');
// bodyType
router.post('/bodytype/save',truckController.save);
router.get('/bodytype/getall',truckController.getall_data);
router.post('/bodytype/update',truckController.update_data);
router.post('/bodytype/delete',truckController.delete_data);
// amenities
router.post('/amenities/save',upload.single("image"),truckController.amenities_save);
router.get('/amenities/getall',truckController.amenities_getall);
router.post('/amenities/update',upload.single("image"),truckController.amenities_update);
router.post('/amenities/delete',truckController.amenities_delete);
// category
router.post('/category/save',upload.single("image"),truckController.category_save);
router.get('/category/getall',truckController.category_getall);
router.post('/category/update',upload.single("image"),truckController.category_update);
router.post('/category/delete',truckController.category_delete);

module.exports =router;