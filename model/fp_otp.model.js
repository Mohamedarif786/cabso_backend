const Sequelize = require('sequelize');

const db = require('../config/db.config');

const Role = db.define('fp_otp',
    {
        user_id: { type: Sequelize.INTEGER, allowNull: false },
        user_email: { type: Sequelize.STRING, allowNull: false, validate: { isEmail: true }, unique: true },
        otp: { type: Sequelize.TEXT, allowNull: false },
        otp_expire_at: { type: Sequelize.TEXT, allowNull: false }
    },
    {
        timestamps: true,
        paranoid: true,
        underscored: true,
    }
);

Role.sync();

module.exports = Role