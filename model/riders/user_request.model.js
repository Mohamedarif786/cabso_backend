const Sequelize = require('sequelize');

const db = require('../../config/db.config');

const Role = db.define('user_request',
    {
        name: { type: Sequelize.STRING, allowNull: false },
        Qty: { type: Sequelize.STRING, allowNull: false},
        price :{type:Sequelize.STRING,allowNull:false},
        status:{type:Sequelize.STRING,allowNull:false},
        user_id: { type: Sequelize.TEXT, allowNull: false },
    },
    {
        timestamps: true,
        paranoid: true,
        underscored: true,
    }
);

Role.sync();

module.exports = Role;