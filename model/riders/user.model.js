const Sequelize = require('sequelize');

const db = require('../../config/db.config');

const Role = db.define('rider_user',
    {
        name: { type: Sequelize.STRING, allowNull: false },
        email: { type: Sequelize.STRING, allowNull: false, validate: { isEmail: true }, unique: true },
        phone:{type:Sequelize.STRING,allowNull:false},
        password: { type: Sequelize.TEXT, allowNull: false },
    },
    {
        timestamps: true,
        paranoid: true,
        underscored: true,
    }
);

Role.sync();

module.exports = Role;