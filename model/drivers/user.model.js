const Sequelize = require('sequelize');

const db = require('../../config/db.config');

const Role = db.define('driver_user',
    {
        name: { type: Sequelize.STRING, allowNull: false },
        email: { type: Sequelize.STRING, allowNull: false, validate: { isEmail: true }, unique: true },
        phone:{type:Sequelize.STRING,allowNull:false},
        password: { type: Sequelize.TEXT, allowNull: false },
        license:{type:Sequelize.STRING,allowNull:false},
        insurance:{type:Sequelize.STRING,allowNull:false},
        status:{type:Sequelize.STRING,allowNull:false},
        truck_image:{type:Sequelize.STRING,allowNull:false}
    },
    {
        timestamps: false,
        paranoid: true,
        underscored: true,
    }
);

Role.sync();

module.exports = Role;