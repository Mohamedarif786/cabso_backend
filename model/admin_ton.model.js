const Sequelize = require('sequelize');
const db = require('../config/db.config');

const Role = db.define('admin_price_settings',
    {
        qty: { type: Sequelize.STRING, allowNull: false },
        price: { type: Sequelize.STRING, allowNull: false },
        user_id: { type: Sequelize.STRING, allowNull: false }
    },
    {
        timestamps: true,
        paranoid: true,
        underscored: true,
    }
);

Role.sync();
module.exports = Role;