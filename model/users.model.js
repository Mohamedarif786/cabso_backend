const Sequelize = require('sequelize');

const db = require('../config/db.config');

const Role = db.define('users',
    {
        first_name: { type: Sequelize.STRING, allowNull: false },
        second_name: { type: Sequelize.STRING, allowNull: false },
        email: { type: Sequelize.STRING, allowNull: false, validate: { isEmail: true }, unique: true },
        password: { type: Sequelize.TEXT, allowNull: false }
    },
    {
        timestamps: true,
        paranoid: true,
        underscored: true,
    }
);

Role.sync();

module.exports = Role;