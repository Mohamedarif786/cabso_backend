const Sequelize = require('sequelize');

const db = require('./../../config/db.config');

const Role = db.define('amenities',
    {
        user_id: { type: Sequelize.INTEGER, allowNull: false },
        title: { type: Sequelize.STRING, allowNull: false},
        Image:{type:Sequelize.STRING,allowNull:false}
    },
    {
        timestamps: true,
        paranoid: true,
        underscored: true,
    }
);

Role.sync();

module.exports = Role