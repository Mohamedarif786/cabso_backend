const Sequelize = require('sequelize');

const db = require('./../../config/db.config');

const Role = db.define('body_type',
    {
        user_id: { type: Sequelize.INTEGER, allowNull: false },
        body_type: { type: Sequelize.STRING, allowNull: false},
    },
    {
        timestamps: true,
        paranoid: true,
        underscored: true,
    }
);

Role.sync();

module.exports = Role