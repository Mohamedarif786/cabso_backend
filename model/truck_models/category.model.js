const Sequelize = require('sequelize');

const db = require('../../config/db.config');

const Role = db.define('category',
    {
        user_id: { type: Sequelize.INTEGER, allowNull: false },
        category_name: { type: Sequelize.STRING, allowNull: false},
        category_description: { type: Sequelize.STRING, allowNull: false},
        body_type: { type: Sequelize.STRING, allowNull: false},
        amenities: { type: Sequelize.STRING, allowNull: false},
        unit_price: { type: Sequelize.STRING, allowNull: false},
        base_price: { type: Sequelize.STRING, allowNull: false},
        category_image: { type: Sequelize.STRING, allowNull: false},
    },
    {
        timestamps: true,
        paranoid: true,
        underscored: true,
    }
);

Role.sync();

module.exports = Role